import sys
from PySide6 import QtWidgets
from analyzer.file_editor import FileEditorApp
from analyzer.network_viewer import NetworkViewerApp
from analyzer.image_info import ImageInfoApp

class MultiFunctionFileAnalyzerApp(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Multi-Function File Analyzer")
        self.setGeometry(100, 100, 800, 600)

        self.tab_widget = QtWidgets.QTabWidget(self)

        self.file_editor_tab = FileEditorApp()
        self.network_viewer_tab = NetworkViewerApp()
        self.image_info_tab = ImageInfoApp()

        self.tab_widget.addTab(self.file_editor_tab, "File Editor")
        self.tab_widget.addTab(self.network_viewer_tab, "Network Viewer")
        self.tab_widget.addTab(self.image_info_tab, "Image Info")

        self.setCentralWidget(self.tab_widget)

def main():
    app = QtWidgets.QApplication([])
    window = MultiFunctionFileAnalyzerApp()
    window.show()
    sys.exit(app.exec())

if __name__ == "__main__":
    main()

