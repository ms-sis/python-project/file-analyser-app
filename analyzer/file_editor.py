import sys
import binascii
from PySide6 import QtWidgets, QtGui
import os

class FileEditorApp(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()
        self.update_in_progress = False

    def init_ui(self):
        self.setWindowTitle("File Editor")
        self.setGeometry(100, 100, 800, 600)

        self.file_path = None  # Store the current file path

        self.file_text = QtWidgets.QPlainTextEdit(self)
        self.hex_text = QtWidgets.QPlainTextEdit(self)

        open_button = QtWidgets.QPushButton("Open File", self)
        open_button.clicked.connect(self.open_file)

        save_button = QtWidgets.QPushButton("Save File", self)
        save_button.clicked.connect(self.save_file)

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addWidget(self.file_text)
        main_layout.addWidget(self.hex_text)

        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addWidget(open_button)
        button_layout.addWidget(save_button)
        main_layout.addLayout(button_layout)

        self.setLayout(main_layout)

        self.file_text.textChanged.connect(self.update_hex_view)
        self.hex_text.textChanged.connect(self.update_file_text)

    def open_file(self):
        file_dialog = QtWidgets.QFileDialog(self, "Open File", "", "All Files (*)")
        if file_dialog.exec_():
            file_path = file_dialog.selectedFiles()[0]
            self.file_path = file_path
            with open(file_path, 'r', encoding='utf-8') as file:
                content = file.read()
                self.file_text.setPlainText(content)

    def save_file(self):
        file_name, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save File", "", "All Files (*);;Text Files (*.txt);;Hex Files (*.hex)")
        if file_name:
            with open(file_name, 'wb') as file:
                hex_content = self.hex_editor.toPlainText().split()
                file_content = bytes([int(value, 16) for value in hex_content])
                file.write(file_content)

    def update_hex_view(self):
        if not self.update_in_progress:
            text = self.file_text.toPlainText()
            hex_text = binascii.hexlify(text.encode('utf-8')).decode('utf-8')
            self.update_in_progress = True
            self.hex_text.setPlainText(hex_text)
            self.update_in_progress = False

    def update_file_text(self):
        if not self.update_in_progress:
            hex_text = self.hex_text.toPlainText()
            try:
                text = binascii.unhexlify(hex_text).decode('utf-8')
                self.update_in_progress = True
                self.file_text.setPlainText(text)
                self.update_in_progress = False
            except Exception as e:
                print("Error:", e)

def main():
    app = QtWidgets.QApplication([])
    window = FileEditorApp()
    window.show()
    sys.exit(app.exec())

if __name__ == "__main__":
    main()
