import sys
import binascii
import requests
from PySide6 import QtWidgets, QtGui

class NetworkViewerApp(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Network Viewer")
        self.setGeometry(100, 100, 800, 600)

        self.url_input = QtWidgets.QLineEdit(self)
        self.http_headers_text = QtWidgets.QTextEdit(self)
        self.text_and_hex_layout = QtWidgets.QHBoxLayout()  # Layout horizontal pour le texte et l'hexadécimal
        self.html_text = QtWidgets.QPlainTextEdit(self)
        self.hex_text = QtWidgets.QPlainTextEdit(self)  # Widget pour afficher l'hexadécimal

        # Réduit la hauteur de la section de l'en-tête en fixant sa hauteur
        self.http_headers_text.setFixedHeight(150)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.url_input)
        layout.addWidget(self.http_headers_text)
        
        self.text_and_hex_layout.addWidget(self.html_text)
        self.text_and_hex_layout.addWidget(self.hex_text)
        layout.addLayout(self.text_and_hex_layout)  # Ajoute le layout horizontal pour le texte et l'hexadécimal

        open_button = QtWidgets.QPushButton("Load URL", self)
        open_button.clicked.connect(self.load_url)

        layout.addWidget(open_button)
        self.setLayout(layout)

    def load_url(self):
        url = self.url_input.text()
        if not url.startswith(("http://", "https://")):
            url = "http://" + url

        try:
            response = requests.get(url)
            http_headers = response.headers
            content = response.text

            self.http_headers_text.setPlainText(str(http_headers))
            self.update_html_view(content)  # Met à jour le widget d'affichage HTML
            self.update_hex_view(content)  # Met à jour le widget d'affichage hexadécimal
        except requests.exceptions.RequestException as e:
            QtWidgets.QMessageBox.critical(self, "Error", str(e))

    def update_html_view(self, text):
        # Affiche le contenu HTML dans le widget dédié
        self.html_text.setPlainText(text)

    def update_hex_view(self, text):
        hex_text = binascii.hexlify(text.encode('utf-8')).decode('utf-8')
        self.hex_text.setPlainText(hex_text)

def main():
    app = QtWidgets.QApplication([])
    window = NetworkViewerApp()
    window.show()
    sys.exit(app.exec())

# if __name__ == "__main__":
#     main()
