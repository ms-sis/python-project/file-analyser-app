import sys
import exifread
import json
import os
from PySide6 import QtWidgets, QtGui, QtCore

class ImageInfoApp(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Image Info Viewer")
        self.setGeometry(100, 100, 800, 600)

        self.image_path = None  # Store the current image path

        self.image_label = QtWidgets.QLabel(self)
        self.exif_text = QtWidgets.QPlainTextEdit(self)

        # Create a QSplitter to display the image and EXIF data side by side
        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        splitter.addWidget(self.image_label)
        splitter.addWidget(self.exif_text)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(splitter)
        open_button = QtWidgets.QPushButton("Open Image", self)
        open_button.clicked.connect(self.open_image)
        export_button = QtWidgets.QPushButton("Export EXIF as JSON", self)
        export_button.clicked.connect(self.export_exif_as_json)
        layout.addWidget(open_button)
        layout.addWidget(export_button)

        self.setLayout(layout)

    def open_image(self):
        image_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Image File", "", "Image Files (*.jpg *.jpeg *.png *.gif *.bmp *.tiff)")
        if image_path:
            self.image_path = image_path
            self.display_image(image_path)
            self.display_exif_info(image_path)

    def display_image(self, image_path):
        pixmap = QtGui.QPixmap(image_path)
        self.image_label.setPixmap(pixmap)

    def display_exif_info(self, image_path):
        with open(image_path, 'rb') as f:
            tags = exifread.process_file(f, details=False)
            exif_data = ""
            for tag in tags.keys():
                exif_data += f"{tag}: {tags[tag]}\n"
            self.exif_text.setPlainText(exif_data)

    def export_exif_as_json(self):
        if self.image_path:
            with open(self.image_path, 'rb') as f:
                tags = exifread.process_file(f, details=False)
                exif_data = {str(tag): str(tags[tag]) for tag in tags.keys()}
                json_filename = os.path.splitext(os.path.basename(self.image_path))[0] + ".json"
                with open(json_filename, "w") as json_file:
                    json.dump(exif_data, json_file, indent=4)
                QtWidgets.QMessageBox.information(self, "Export Successful", f"EXIF data has been exported as {json_filename}.")

def main():
    app = QtWidgets.QApplication([])
    window = ImageInfoApp()
    window.show()
    sys.exit(app.exec())

# if __name__ == "__main__":
#     main()
