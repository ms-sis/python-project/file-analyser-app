# file-analyser-app

## Description

C'est une application Python qui permet aux utilisateurs d'afficher et d'analyser des fichiers locaux, des pages web, et des images en affichant leurs données hexadécimales et les informations EXIF. L'application utilise PySide6 pour l'interface graphique, requests pour les requêtes HTTP, et Pillow pour le traitement des images.

## Fonctionnalités

L'application propose les fonctionnalités suivantes :

- **Éditeur Hexadécimal de Fichiers Locaux** : Permet d'ouvrir un fichier local, d'afficher son contenu et son équivalent hexadécimal. Les modifications apportées dans l'un des panneaux sont répercutées dans l'autre, et il est possible de sauvegarder les modifications.

- **Visualiseur Hexadécimal de Fichiers Réseau (HTTP/HTTPS)** : Permet de charger une page web via une URL, d'afficher les en-têtes HTTP et l'équivalent hexadécimal de la réponse.

- **Visualisation des Données EXIF** : Pour les images, l'application affiche les données EXIF si elles existent et permet d'exporter ces données au format JSON.

## Configuration Requise

Pour exécuter l'application, assurez-vous d'avoir installé Python 3 et les bibliothèques requises. Vous pouvez les installer en utilisant `pip` :
```shell
pip install -r requirements.txt
```
/!\ Il se peut que vous soyez amené à changer la version de PySide6.

## Utilsation

Pour lancer l'application, Placez vous à la racine du projet et utilisez la commande suivante :
```shell
python3 main.py
```

## Bug Connus

- Lors de l'ouverture de l'image, elle n'est pas redimenssionnée ce qui peut entraîner des problèmes de rendu.

## Fonctionnalités à Ajouter

- Modification des fichiers en ligne